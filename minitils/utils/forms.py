
from django import forms

class BoxForm(forms.Form):
    width = forms.FloatField(min_value=0)
    height = forms.FloatField(min_value=0)
    depth = forms.FloatField(min_value=0)
