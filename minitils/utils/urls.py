from django.conf.urls import url

from . import views

urlpatterns = [

    url(r'^$', views.index),
    url(r'^box/$', views.box_template, name='box_template'),
    
]
