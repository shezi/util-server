from django.shortcuts import render
from django.http import HttpResponse

import svgwrite
from svgwrite import cm
from svgwrite.shapes import Line

from .forms import BoxForm

def index(request):
    return render(request, 'index.html')


def box_template(request):
    ctx = {}
    ctx['form'] = BoxForm()

    if request.method == 'POST':

        ctx['form'] = form = BoxForm(request.POST)
        if form.is_valid():

         def l(g, x_s, y_s, x_e, y_e):
             g.add(Line(start=(x_s*cm, y_s*cm), end=(x_e*cm, y_e*cm)))

         w = form.cleaned_data['width']
         h = form.cleaned_data['height']
         d = form.cleaned_data['depth']

         x0 = 0
         x1 = d/2
         x2 = d/2 * 3
         x3 = d/2 * 3 + w
         x4 = d/2 * 5 + w
         x5 = 3*d + w

         y0 = 0
         y1 = d/2
         y2 = 3*d/2
         y3 = 3*d/2 + h
         y4 = 5*d/2 + h
         y5 = 3*d + h

         dwg = svgwrite.Drawing(size=(x5*cm, y5*cm))

         bottom = dwg.add(dwg.g(id='bottom', stroke='green'))

         # central rect
         l(bottom, x2, y2, x3, y2)
         l(bottom, x2, y2, x2, y3)
         l(bottom, x2, y3, x3, y3)
         l(bottom, x3, y2, x3, y3)

         # top flap
         l(bottom, x2, y2, x2, y1)
         l(bottom, x2, y1, x3, y1)
         l(bottom, x3, y1, x3, y2)
         l(bottom, x2, y1, x2, y0)
         l(bottom, x2, y0, x3, y0)
         l(bottom, x3, y0, x3, y1)

         # bottom flap
         l(bottom, x2, y3, x2, y4)
         l(bottom, x2, y4, x3, y4)
         l(bottom, x3, y4, x3, y3)

         l(bottom, x2, y4, x2, y5)
         l(bottom, x2, y5, x3, y5)
         l(bottom, x3, y5, x3, y4)

         # left flap
         l(bottom, x2, y2, x1, y2)
         l(bottom, x1, y2, x1, y3)
         l(bottom, x1, y3, x2, y3)
         l(bottom, x1, y2, x0, y2)
         l(bottom, x0, y2, x0, y3)
         l(bottom, x0, y3, x1, y3)
         # left flap wing
         l(bottom, x3, y2, x3+1, y2-1)
         l(bottom, x3+1, y2-1, x4, y2-1)
         l(bottom, x4, y2-1, x4, y2)
         l(bottom, x3, y3, x3+1, y3+1)
         l(bottom, x3+1, y3+1, x4, y3+1)
         l(bottom, x4, y3+1, x4, y3)


         # right flap
         l(bottom, x3, y2, x4, y2)
         l(bottom, x4, y2, x4, y3)
         l(bottom, x4, y3, x3, y3)

         l(bottom, x4, y2, x5, y2)
         l(bottom, x5, y2, x5, y3)
         l(bottom, x5, y3, x4, y3)
         # right flap wing
         l(bottom, x2, y2, x2-1, y2-1)
         l(bottom, x2-1, y2-1, x1, y2-1)
         l(bottom, x1, y2-1, x1, y2)
         l(bottom, x2, y3, x2-1, y3+1)
         l(bottom, x2-1, y3+1, x1, y3+1)
         l(bottom, x1, y3+1, x1, y3)

         response = HttpResponse(content_type='image/svg+xml')
         response['Content-Disposition'] = 'attachment; filename="box_template_{}x{}x{}.svg"'.format(w, h ,d)
         dwg.write(response)
         return response
         # dwg.save()

    return render(request, 'utils/box_template.djhtml', ctx)

